# Simple Material CSS

The easy way to make your website beatiful.

## Requirements

- Web browser
- Google fonts (optional)

## Instalation

Actually there is no way to install the CSS but you can clone the repository and copy the files:

```
$ git clone https://gitlab.com/agosto182/simplematerialcss.git
$ cd simplematerialcss
$ cp -r * YOUR_DESTINATION_PATH
```

Or more smart making a systemlink (MacOS and Linux systems):

```
$ ln -s simplematerialcss YOUR_DESTINATION_PATH
```

## Some images

**WIP**

## About

**WIP**

## Author

Ivan Agosto

**WIP**

## License

[GPLv3](LICENSE)

**WIP**
